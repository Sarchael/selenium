from selenium import webdriver

with webdriver.Chrome() as driver:
    driver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm")
    driver.find_element_by_name("seven").click()
    driver.find_element_by_name("mul").click()
    driver.find_element_by_name("three").click()
    driver.find_element_by_name("result").click()
    if driver.find_element_by_name("Display").get_attribute("value") != "21":
        raise Exception
    driver.save_screenshot("screen.png")
    driver.quit()
